import React, { useState, ChangeEvent } from "react";
import TodoTask from "./Components/TodoTask";
import { ITask } from "./Interfaces";
import './App.css';

const App = () => {
  const checkStorage = localStorage.getItem('listOfTodos')
  
  const storageAvai = () =>{
    if (!checkStorage) {
    return []
  } else { return JSON.parse(checkStorage)}}

  const [task, setTask] = useState<string>("");
  const [deadLine, setDeadLine] = useState<number>(0);
  const [todo, setTodo] = useState<ITask[]>((storageAvai));

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.name == 'task') {
      setTask(e.target.value);
    } else {
      setDeadLine(Number(e.target.value));
    }
  }

  const addTask = () => {
    // const task2 = document.querySelector('input[name=task]');
    const newTask = {
      taskName: task,
      deadLine: deadLine
    }
    setTodo(prev =>{
      const newTodo = [...todo, newTask]
    
      // lưu Local Storage
      const jsonTodo = JSON.stringify(newTodo)
      localStorage.setItem('listOfTodos',jsonTodo)

      return newTodo
    });
    setTask("");
    setDeadLine(0);
  }

  const completeTask = (taskNameToDelete: string) => {
    const newTodo = todo.filter(task => {
      return task.taskName != taskNameToDelete
    })

    setTodo(newTodo)
    const jsonTodo = JSON.stringify(newTodo)
    localStorage.setItem('listOfTodos',jsonTodo)
  }

  return (
    <div className="App">
      <div className="header">
        <div className="inputContainer">
          <input value={task} type="text" name="task" placeholder="Add a task" onChange={handleChange}/>
          <input value={deadLine} type="number" name="deadLine" placeholder="Set a deadline" onChange={handleChange} />
        </div>
        <button onClick={addTask}>Add</button>
      </div>
      <div className="todoList">
        {todo.map((task: ITask, key: number) => {
          return (<TodoTask key={key} task={task} completeTask={completeTask} />)
        })}
      </div>
    </div>
  )
}

export default App;