import React from "react";
import { ITask } from "../Interfaces";

interface Props{
    task:ITask;
    completeTask(taskNameToDelete:string):void;
}

const TodoTask = ({task, completeTask}: Props)=>{
    return(
        <div className="task">
            <div className="content">
                <span>{task.taskName}</span>
                <span>{task.deadLine}</span>
            </div>
            <button onClick={()=>
                completeTask(task.taskName)
            }>X</button>
        <TodoTask2></TodoTask2>
        </div>
    )
}
const TodoTask2 = ()=>{
    return(
        <p>aaa</p>
    )
}

export default TodoTask;